import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Label {
    id: label
    color: "white"; text: "HaQton!"
    SequentialAnimation on scale {
        loops: Animation.Infinite
        PropertyAnimation { to: 0.25; duration: 1500; easing.type: Easing.OutElastic }
        PropertyAnimation { to: 1; duration: 1500; easing.type: Easing.OutElastic }
        PauseAnimation { duration: 10000 }
    }
    SequentialAnimation on rotation {
        loops: Animation.Infinite
        PropertyAnimation { to: -45; duration: 1000; easing.type: Easing.OutElastic }
        PropertyAnimation { to: 45; duration: 1000; easing.type: Easing.OutElastic }
        PropertyAnimation { to: 0; duration: 1000; easing.type: Easing.OutElastic }
        PauseAnimation { duration: 10000 }
    }
}

