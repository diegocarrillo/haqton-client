import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/FontAwesome"

Button {
    id: button

    width: 100; height: 60

    Label {
        id: textLabel
        anchors.fill: parent
        font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 36}
        text: Icons.faArrowLeft
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: mainRect.color
        wrapMode: Text.WordWrap
    }

    Material.background: "white"
}
