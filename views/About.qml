import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {

    ReturnButton{
        id: returnButton
        anchors{top:parent.top; left: parent.left}
        onClicked: mainStackView.replace("qrc:/views/MainMenu.qml");
    }

    Label{
        id: aboutext
        anchors{top:returnButton.bottom; topMargin: internal.margins; horizontalCenter: parent.horizontalCenter}
        width: parent.width
        horizontalAlignment: Text.AlignHLeft

        text: "Sobre o HaQton:"
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 40}
    }

    Rectangle {
        id: aboutRectangle
        anchors {top: aboutext.bottom; topMargin: internal.margins*2;
            bottom: parent.bottom; bottomMargin: internal.margins;
            left: parent.left; leftMargin: internal.margins;
            right: parent.right; rightMargin: internal.margins}

        radius: 2
        color: "white"

        Text {
            anchors.fill: parent
            anchors.margins: internal.margins
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            font.pixelSize: 20
            text: 'O HaQton é uma competição de programação realizada como parte da QtCon Brasil 2020, \
e tem o objetivo de promover o uso do Qt na comunidade Brasileira e Latino-Americana.\n\n\
Nesta competição, cada equipe deve desenvolver uma aplicação Qt para um jogo de perguntas e respostas, \
cujo tema é o próprio universo Qt!\n\n\Este aplicativo é o resultado do desenvolvimento de Diego Carrillo.\
\n\nEspero que você, jogador, divirta-se! :)'
        }
    }

}
