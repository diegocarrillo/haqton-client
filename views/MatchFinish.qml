import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {

    Label{
        id: finishText
        height: contentHeight
        width: parent.width
        anchors {top: parent.top; topMargin: internal.margins; horizontalCenter: parent.horizontalCenter}

        text: "Fim da partida!"
        horizontalAlignment: Text.AlignHLeft
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 40}
    }

    Label{
        id: resultText
        height: contentHeight
        width: parent.width
        anchors {top: finishText.bottom; topMargin: internal.margins; horizontalCenter: parent.horizontalCenter}

        text: "Resultado:"
        horizontalAlignment: Text.AlignHLeft
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 32}
    }

    ListView
    {
        id: matchSummary
        width: parent.width
        anchors {top: resultText.bottom; topMargin: internal.margins;
            bottom: returnButton.top; bottomMargin: internal.margins;
            horizontalCenter: parent.horizontalCenter}

        spacing: internal.margins/2
        model: Core.gameManager.context.matchSummary
        clip: true

        ScrollBar.vertical: ScrollBar {}

        delegate: Rectangle {
            width: parent.width
            height: 60
            radius: 2
            color: "white"

            Label {
                id: userSummaryIcon
                width: 50; height: parent.height
                anchors {left: parent.left; leftMargin: internal.margins; verticalCenter: parent.verticalCenter}
                font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 40}
                text: modelData.is_winner ?
                          Icons.faTrophy : Icons.faUser

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: mainRect.color
            }

            ColumnLayout{
                spacing: 0
                anchors{left: userSummaryIcon.right; leftMargin: internal.margins; right: parent.right;
                    verticalCenter: parent.verticalCenter;}
                Text {
                    text: modelData.player_name
                    font.pixelSize: 18
                }
                Text {
                    text: "Pontuação: " + modelData.current_score
                    font.pixelSize: 12
                }
                Text {
                    visible: modelData.is_winner
                    text: "Vencedor"
                    font.pixelSize: 12
                }
            }
        }
    }

    GameButton {
        id: returnButton

        width: parent.width; height: internal.buttonHeight
        anchors {bottom: parent.bottom; bottomMargin: internal.margins; horizontalCenter: parent.horizontalCenter}

        iconLabel { text: Icons.faChevronCircleRight; color: mainRect.color }
        text: "Voltar ao menu principal"

        onClicked:{
            mainStackView.replace("qrc:/views/MainMenu.qml")
        }
    }
}
