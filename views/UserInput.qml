import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {
    ColumnLayout {
        width: parent.width
        anchors { verticalCenter: parent.verticalCenter; verticalCenterOffset: -haqtonLabel.height }
        spacing: internal.margins*4

        HaQtonLabel {
            id: haqtonLabel
            Layout.topMargin: 4*internal.margins
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
        }

        Rectangle {
            id: nicknameInput
            property alias text: nicknameTextField.text
            Layout.fillWidth: true; Layout.preferredHeight: 80
            radius: 2

            RowLayout{
                anchors.fill: parent

                Label {
                    Layout.preferredWidth: 80; Layout.fillHeight: true
                    Layout.alignment: Qt.AlignCenter
                    font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 48}
                    text: Icons.faUser
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: mainRect.color

                }

                TextField {
                    id: nicknameTextField
                    Layout.fillWidth: true; Layout.fillHeight: true
                    Layout.alignment: Qt.AlignCenter
                    font.pixelSize: 18
                    color: "#1b1b1b"
                    placeholderText: "Insira seu nickname"
                    background: Rectangle {}
                }
            }
        }

        Rectangle {
            id: emailInput
            property alias text: emailTextField.text
            Layout.fillWidth: true; Layout.preferredHeight: 80
            radius: 2

            RowLayout{
                anchors.fill: parent

                Label {
                    Layout.preferredWidth: 80; Layout.fillHeight: true
                    Layout.alignment: Qt.AlignCenter
                    font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 48}
                    text: Icons.faAt
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: mainRect.color
                }

                TextField {
                    id: emailTextField
                    Layout.fillWidth: true; Layout.fillHeight: true
                    Layout.alignment: Qt.AlignCenter
                    font.pixelSize: 18
                    color: "#1b1b1b"
                    placeholderText: "Insira seu e-mail"
                    inputMethodHints: Qt.ImhEmailCharactersOnly
                    background: Rectangle {}
                }
            }
        }
    }

    GameButton {
        visible: nicknameInput.text !== "" && emailInput.text !== ""
        width: parent.width; height: internal.buttonHeight
        anchors {bottom: parent.bottom; bottomMargin: internal.margins; horizontalCenter: parent.horizontalCenter}
        iconLabel { text: Icons.faChevronCircleRight; color: mainRect.color }
        text: "Ir ao menu principal"

        onPressed:{
            Core.playerModel.nickname = nicknameInput.text
            Core.playerModel.email = emailInput.text
            mainStackView.replace("qrc:/views/MainMenu.qml")
        }
    }
}
