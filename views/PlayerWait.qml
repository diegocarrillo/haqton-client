import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {

    ColumnLayout {
        anchors{top:parent.top; topMargin: internal.margins;
            left: parent.left; leftMargin: internal.margins
            right: parent.right; rightMargin: internal.margins;
            bottom: waitMatchText.top; bottomMargin: internal.margins}

        spacing: internal.margins

        Label{
            id: textLabel
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            horizontalAlignment: Text.AlignHLeft

            text: "Aguarde o início da partida"
            color: "white"
            wrapMode: Text.WordWrap
            font.capitalization: Font.AllUppercase
            font { family: gameFont.name; pixelSize: 40}
        }

        ListView
        {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            model: Core.gameManager.context.currentMatch.match_players
            clip: true

            ScrollBar.vertical: ScrollBar {}

            delegate: Rectangle {
                width: parent.width
                height: 80
                radius: 2

                Label {
                    id: userIcon
                    width: 50; height: parent.height
                    anchors {left: parent.left; leftMargin: internal.margins; verticalCenter: parent.verticalCenter}
                    font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 40}
                    text: modelData.approved ? Icons.faUserCheck : Icons.faUserClock
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: mainRect.color
                }


                Text {
                    id: playerName
                    anchors{left: userIcon.right; leftMargin: internal.margins; right: parent.right;
                        verticalCenter: parent.verticalCenter; verticalCenterOffset: -internal.margins}
                    text: modelData.player_name
                    font.pixelSize: 18
                }
                Text {
                    id: playerStatus
                    anchors{left: userIcon.right; leftMargin: internal.margins; right: parent.right;
                        top: playerName.bottom; topMargin: internal.margins/2}
                    text: modelData.approved ? "aprovado" : "aguardando aprovação"
                    font.pixelSize: 12
                }
            }

            onModelChanged:{
                //Verify if player is still at the current match or got kicked from it
                var isPlayerAtCurrentMatch = false;
                var matchPlayers = Core.gameManager.context.currentMatch.match_players
                for(var i = 0; i < matchPlayers.length; i++){
                    if(matchPlayers[i].player_name === Core.playerModel.nickname){
                        isPlayerAtCurrentMatch = true;
                        break;
                    }
                }

                if(!isPlayerAtCurrentMatch){
                    mainStackView.replace("qrc:/views/MainMenu.qml")
                }
            }
        }
    }

    Label{
        id: waitMatchText
        height: contentHeight
        width: parent.width
        anchors { bottom: parent.bottom; bottomMargin:  internal.margins*2;
            horizontalCenter: parent.horizontalCenter;}

        text: "Aguarde o dono da partida iniciar o jogo..."
        horizontalAlignment: Text.AlignHLeft
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 32}
    }

    Connections{
        target: Core.gameManager
        function onNewQuestionReceived(){
            mainStackView.replace("qrc:/views/MatchRound.qml")
        }
    }
}
