import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {

    ColumnLayout {
        anchors.fill: parent
        spacing: internal.margins

        Label{
            id: textLabel
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            horizontalAlignment: Text.AlignHLeft

            text: "Aguardando chegada dos jogadores"
            color: "white"
            wrapMode: Text.WordWrap
            font.capitalization: Font.AllUppercase
            font { family: gameFont.name; pixelSize: 40}
        }

        ListView
        {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            model: Core.gameManager.context.currentMatch.match_players
            clip: true

            ScrollBar.vertical: ScrollBar {}

            delegate: Rectangle {
                width: parent.width
                height: 80
                radius: 2

                Label {
                    id: userIcon
                    width: 50; height: parent.height
                    anchors {left: parent.left; leftMargin: internal.margins; verticalCenter: parent.verticalCenter}
                    font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 40}
                    text: modelData.approved ? Icons.faUserCheck : Icons.faUserClock
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: mainRect.color
                }


                Text {
                    id: playerName
                    anchors{left: userIcon.right; leftMargin: internal.margins; right: parent.right;
                        verticalCenter: parent.verticalCenter; verticalCenterOffset: -internal.margins}
                    text: modelData.player_name
                    font.pixelSize: 18
                }
                Text {
                    id: playerStatus
                    anchors{left: userIcon.right; leftMargin: internal.margins; right: parent.right;
                        top: playerName.bottom; topMargin: internal.margins/2}
                    text: modelData.approved ? "aprovado" : "aguardando aprovação"
                    font.pixelSize: 12
                }

                Button{
                    id: approveButton
                    visible: !modelData.approved

                    width: 60; height: parent.height
                    anchors{right: declineButton.left; rightMargin: internal.margins/2}

                    Material.background: "white"

                    Label{
                        anchors.fill: parent

                        text: Icons.faCheck
                        color: mainRect.color
                        font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 50}
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }

                    onClicked: Core.gameManager.updatePlayerStatus(Core.gameManager.context.currentMatch.id,
                                                                   modelData.id,
                                                                   true)
                }

                Button{
                    id: declineButton
                    visible: !modelData.approved

                    width: 60; height: parent.height
                    anchors{right: parent.right; rightMargin: internal.margins/2}

                    Material.background: "white"

                    Label{
                        anchors.fill: parent

                        text: Icons.faTimes
                        color: mainRect.color
                        font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 50}
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }

                    onClicked: Core.gameManager.updatePlayerStatus(Core.gameManager.context.currentMatch.id,
                                                                   modelData.id,
                                                                   false)
                }
            }
        }

        GameButton {
            id: startGameButton
            visible: areAllPlayersReady()

            Layout.fillWidth: true
            Layout.preferredHeight: 100
            iconLabel { text: Icons.faChevronCircleRight; color: mainRect.color }
            text: "Iniciar partida"

            onClicked:{
                Core.gameManager.startMatch(Core.gameManager.context.currentMatch.id)
                mainStackView.replace("qrc:/views/MatchRound.qml")
            }

            function areAllPlayersReady(){
                var matchPlayers = Core.gameManager.context.currentMatch.match_players
                var numberOfPlayersReady = 0;

                if(matchPlayers === undefined){
                    return false;
                }

                for(var i = 0; i < matchPlayers.length; i++){
                    if(matchPlayers[i].approved){
                        numberOfPlayersReady ++;
                    }
                }

                return (numberOfPlayersReady === matchPlayers.length && matchPlayers.length > 1);
            }
        }
    }
}
