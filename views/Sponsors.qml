import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {

    ReturnButton{
        id: returnButton
        anchors{top:parent.top; left: parent.left}
        onClicked: mainStackView.replace("qrc:/views/MainMenu.qml");
    }

    Label{
        id: sponsorsText
        anchors{top:returnButton.bottom; topMargin: internal.margins; horizontalCenter: parent.horizontalCenter}
        width: parent.width
        horizontalAlignment: Text.AlignHLeft

        text: "Patrocinadores:"
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 40}
    }

    ColumnLayout
    {
        id: sponsorsList
        anchors{top: sponsorsText.bottom; topMargin: internal.margins; horizontalCenter: parent.horizontalCenter}
        width: parent.width
        Layout.fillWidth: true; Layout.fillHeight: true
        spacing: internal.margins

        Repeater {
            model: [
                {
                    name: "Qmob Solutions",
                    logo: "qrc:/assets/images/qmob-logo.png",
                    backgroundColor: "#00465f"
                },
                {
                    name: "openSUSE",
                    logo: "qrc:/assets/images/opensuse-logo.png",
                    backgroundColor: "#143C52"
                },
                {
                    name: "Toradex",
                    logo: "qrc:/assets/images/toradex-logo.png",
                    backgroundColor: "white"
                },
                {
                    name: "B2Open Systems",
                    logo: "qrc:/assets/images/b2open-logo.png",
                    backgroundColor: "#820099"
                }
            ]

            Rectangle {
                id: sponsor
                width: parent.width
                height: 120
                radius: 2
                color: modelData.backgroundColor

                Rectangle {
                    id: sponsorName
                    width: parent.width
                    height: 24
                    radius: 2
                    color: "white"
                    Text {
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: modelData.name
                        font.pixelSize: 20
                    }
                }

                Image {
                    anchors {top: sponsorName.bottom; topMargin: internal.margins;
                    bottom: sponsor.bottom; bottomMargin: internal.margins;
                    left: sponsor.left; leftMargin: internal.margins;
                    right: sponsor.right; rightMargin: internal.margins}

                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    source: modelData.logo
                }
            }
        }
    }


}
