import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {

    property bool isRoundFinished: didAllPlayersAnswer()
    property bool isQuestionAnswered: false
    property bool isMatchFinished: false

    Label{
        id: questionLabel
        visible: !isMatchFinished
        height: contentHeight
        width: parent.width
        anchors {top: parent.top; topMargin: internal.margins; horizontalCenter: parent.horizontalCenter}

        text: "Pergunta: " + Core.gameManager.context.question.description
        horizontalAlignment: Text.AlignHLeft
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 32}
    }

    ListView
    {
        id: questionOptionsList
        visible: !isRoundFinished && !isQuestionAnswered && !isMatchFinished
        height: contentHeight
        width: parent.width
        anchors {top: questionLabel.bottom; topMargin: internal.margins; horizontalCenter: parent.horizontalCenter}
        spacing: internal.margins/2
        model: Core.gameManager.context.question.question_options
        clip: true

        onModelChanged: isQuestionAnswered = false

        delegate: Button {
            id: questionOptionButton
            opacity: enabled ? 1 : 0.7
            width: parent.width
            height: 80
            Material.background: "white"

            contentItem: Item{
                Label{
                    id: optionIndex
                    anchors{
                        left: parent.left; leftMargin: internal.margins
                        verticalCenter: parent.verticalCenter
                    }
                    height: parent.height
                    width: 42

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    text: (index + 1) + "."
                    color: mainRect.color
                    font { family: gameFont.name; pixelSize: 40}
                }

                Text {
                    id: optionText
                    text: modelData.description
                    font.pixelSize: 18
                    anchors.fill: parent

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }

            onPressed:{
                isQuestionAnswered = true;

                //Get current player id
                var currentPlayerId
                var matchPlayers = Core.gameManager.context.currentMatch.match_players
                for(var i = 0; i < matchPlayers.length; i++){
                    if(matchPlayers[i].player_name === Core.playerModel.nickname){
                        currentPlayerId = matchPlayers[i].id
                        break;
                    }
                }

                Core.gameManager.processPlayerAnswer(Core.gameManager.context.currentMatch.id,
                                                     currentPlayerId,
                                                     modelData.question_id,
                                                     index)
            }
        }
    }

    Label{
        id: waitOtherPlayers
        visible: !isRoundFinished && isQuestionAnswered && !isMatchFinished
        width: parent.width
        anchors {top: questionLabel.bottom; topMargin: internal.margins;
            bottom: separator1.top; bottomMargin: internal.margins;
            horizontalCenter: parent.horizontalCenter}

        text: "Aguarde os outros jogadores responderem..."
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 24}
    }

    Rectangle{
        id: separator1
        visible: !isRoundFinished && !isMatchFinished
        height: 2
        anchors {top: questionOptionsList.bottom; topMargin: internal.margins;
            left: parent.left; leftMargin: internal.margins/2;
            right: parent.right; rightMargin: internal.margins/2;}

        color: "white"
    }

    Label{
        id: playerStatusLabel
        visible: !isRoundFinished && !isMatchFinished

        height: contentHeight
        width: parent.width
        anchors {top: separator1.bottom; topMargin: internal.margins; horizontalCenter: parent.horizontalCenter}

        text: "Status dos jogadores:"
        horizontalAlignment: Text.AlignHLeft
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 32}
    }

    ListView
    {
        id: playerStatusList
        visible: !isRoundFinished && !isMatchFinished
        width: parent.width
        anchors {top: playerStatusLabel.bottom; topMargin: internal.margins;
            bottom: parent.bottom; bottomMargin: internal.margins;
            horizontalCenter: parent.horizontalCenter}

        spacing: internal.margins/2
        model: Core.gameManager.context.currentRound
        clip: true

        ScrollBar.vertical: ScrollBar {}

        delegate: Rectangle {
            width: parent.width
            height: 60
            radius: 2
            color: "white"

            Label {
                id: userIcon
                width: 50; height: parent.height
                anchors {left: parent.left; leftMargin: internal.margins; verticalCenter: parent.verticalCenter}
                font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 40}
                text: modelData.has_played ? Icons.faUserCheck : Icons.faUserClock
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: mainRect.color
            }

            ColumnLayout{
                spacing: 0
                anchors{left: userIcon.right; leftMargin: internal.margins; right: parent.right;
                    verticalCenter: parent.verticalCenter;
                }
                Text {
                    text: modelData.player_name
                    font.pixelSize: 18
                }
                Text {
                    text: "Pontuação: " + modelData.current_score
                    font.pixelSize: 12
                }
                Text {
                    text: modelData.has_played ? "participante já respondeu" : "participante ainda não respondeu"
                    font.pixelSize: 12
                }
            }
        }
    }

    Rectangle {
        id: correctAnswer
        visible: isRoundFinished && !isMatchFinished
        width: parent.width
        height: 60
        anchors {top: questionLabel.bottom; topMargin: internal.margins*4; horizontalCenter: parent.horizontalCenter}

        radius: 2
        color: "white"

        Label {
            id: correctAnswerIcon
            width: 50; height: parent.height
            anchors {left: parent.left; leftMargin: internal.margins; verticalCenter: parent.verticalCenter}
            font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 40}
            text: Icons.faCheckCircle
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: mainRect.color
        }

        Text {
            id: correctAnswerText
            height: parent.height
            anchors {left: correctAnswerIcon.right; leftMargin: internal.margins;
                right: parent.right; verticalCenter: parent.verticalCenter}

            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            text: "A resposta correta era: " +
                  roundSummary.getAnsweredQuestionText(Core.gameManager.context.question.right_option)
            font.pixelSize: 18
        }
    }

    Rectangle{
        id: separator2
        visible: isRoundFinished && !isMatchFinished
        height: 2
        anchors {top: correctAnswer.bottom; topMargin: internal.margins*4;
            left: parent.left; leftMargin: internal.margins/2;
            right: parent.right; rightMargin: internal.margins/2;}

        color: "white"
    }

    Label{
        id: roundStatusLabel
        visible: isRoundFinished && !isMatchFinished

        height: contentHeight
        width: parent.width
        anchors {top: separator2.bottom; topMargin: internal.margins*4; horizontalCenter: parent.horizontalCenter}

        text: "Status do round:"
        horizontalAlignment: Text.AlignHLeft
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 32}
    }

    ListView
    {
        id: roundSummary
        visible: isRoundFinished && !isMatchFinished
        width: parent.width
        anchors {top: roundStatusLabel.bottom; topMargin: internal.margins;
            bottom: nextRoundButton.top; bottomMargin: internal.margins;
            horizontalCenter: parent.horizontalCenter}

        spacing: internal.margins/2
        model: Core.gameManager.context.currentRound
        clip: true

        ScrollBar.vertical: ScrollBar {}

        delegate: Rectangle {
            width: parent.width
            height: 60
            radius: 2
            color: "white"

            Label {
                id: userSummaryIcon
                width: 50; height: parent.height
                anchors {left: parent.left; leftMargin: internal.margins; verticalCenter: parent.verticalCenter}
                font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 40}
                text: modelData.question_answered === Core.gameManager.context.question.right_option ?
                          Icons.faUserCheck : Icons.faUserTimes

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: mainRect.color
            }

            ColumnLayout{
                spacing: 0
                anchors{left: userSummaryIcon.right; leftMargin: internal.margins; right: parent.right;
                    verticalCenter: parent.verticalCenter;}
                Text {
                    text: modelData.player_name
                    font.pixelSize: 18
                }
                Text {
                    text: "Pontuação: " + modelData.current_score
                    font.pixelSize: 12
                }
                Text {
                    text: "Resposta: " + roundSummary.getAnsweredQuestionText(modelData.question_answered)
                    font.pixelSize: 12
                }
            }
        }

        function getAnsweredQuestionText(answeredQuestionIndex)
        {
            var questionOptions = Core.gameManager.context.question.question_options
            if(questionOptions === undefined){
                return "";
            }
            else if(questionOptions[answeredQuestionIndex] === undefined)
            {
                return "";
            }
            else
            {
                return questionOptions[answeredQuestionIndex].description
            }
        }
    }

    GameButton {
        id: finishMatchButton
        visible: isRoundFinished && Core.playerModel.isMatchCreator && !isMatchFinished
        width: parent.width/2 - internal.margins/2; height: internal.buttonHeight
        anchors {bottom: parent.bottom; bottomMargin: internal.margins;
            right: parent.right}

        text: "Encerrar partida"
        iconLabel { text: Icons.faFastForward; color: mainRect.color }

        onClicked:{
            Core.gameManager.endMatch(Core.gameManager.context.currentMatch.id)
        }
    }

    GameButton {
        id: nextRoundButton
        visible: isRoundFinished && Core.playerModel.isMatchCreator && !isMatchFinished
        width: parent.width/2 - internal.margins/2; height: internal.buttonHeight
        anchors {bottom: parent.bottom; bottomMargin: internal.margins;
            left: parent.left}

        text: "Próxima pergunta"
        iconLabel { text: Icons.faStepForward; color: mainRect.color }

        onClicked:{
            Core.gameManager.getRandomQuestion(Core.gameManager.context.currentMatch.id)
        }
    }

    GameButton {
        id: seeResultsButton
        visible: isMatchFinished

        width: parent.width; height: internal.buttonHeight
        anchors {bottom: parent.bottom; bottomMargin: internal.margins; horizontalCenter: parent.horizontalCenter}

        text: "Ver resultado final"
        iconLabel { text: Icons.faTrophy; color: mainRect.color }

        onClicked:{
            mainStackView.replace("qrc:/views/MatchFinish.qml")
        }
    }

    Label{
        id: matchFinishedLabel
        visible: isMatchFinished
        anchors {verticalCenter: parent.verticalCenter; verticalCenterOffset: -seeResultsButton.height;
            horizontalCenter: parent.horizontalCenter}

        text: "Fim da partida!"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 44}
    }

    Label{
        id: waitNextRoundText
        visible: isRoundFinished && !Core.playerModel.isMatchCreator && !isMatchFinished
        height: contentHeight
        width: parent.width
        anchors { bottom: parent.bottom; bottomMargin:  internal.margins*2;
            horizontalCenter: parent.horizontalCenter;}

        text: "Aguarde o dono da partida continuar o jogo..."
        horizontalAlignment: Text.AlignHLeft
        color: "white"
        wrapMode: Text.WordWrap
        font.capitalization: Font.AllUppercase
        font { family: gameFont.name; pixelSize: 32}
    }

    function didAllPlayersAnswer()
    {
        var matchPlayers = Core.gameManager.context.currentRound

        if(matchPlayers.length === 0)
            return false;

        var numberOfPlayersThatAnswered = 0;

        for(var i = 0; i < matchPlayers.length; i++){
            if(matchPlayers[i].has_played){
                numberOfPlayersThatAnswered ++;
            }
        }

        return (numberOfPlayersThatAnswered === matchPlayers.length);
    }

    Connections{
        target: Core.gameManager
        function onMatchFinished(){
            isMatchFinished = true
        }
    }
}
