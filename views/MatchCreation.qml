import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {

    ReturnButton{
        anchors{top:parent.top; left: parent.left}
        onClicked: mainStackView.replace("qrc:/views/MainMenu.qml");
    }

    ColumnLayout {
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter
        spacing: internal.margins*4

        Label{
            id: textLabel
            Layout.fillWidth: true
            Layout.preferredHeight: 40
            horizontalAlignment: Text.AlignHLeft

            text: "Nova partida"
            color: "white"
            wrapMode: Text.WordWrap
            font.capitalization: Font.AllUppercase
            font { family: gameFont.name; pixelSize: 40}
        }

        Rectangle {
            id: matchNameInput
            property alias text: matchTextField.text
            Layout.fillWidth: true; Layout.preferredHeight: 80
            radius: 2

            RowLayout{
                anchors.fill: parent

                Label {
                    Layout.preferredWidth: 80; Layout.fillHeight: true
                    Layout.alignment: Qt.AlignCenter
                    font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 48}
                    text: Icons.faPlusCircle
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: mainRect.color
                }

                TextField {
                    id: matchTextField
                    Layout.fillWidth: true;
                    Layout.alignment: Qt.AlignCenter
                    font.pixelSize: 18
                    color: "#1b1b1b"
                    background: Rectangle {}
                    placeholderText: "Insira o nome da partida"
                }
            }
        }
    }

    GameButton {
        visible: matchNameInput.text !== ""
        width: parent.width; height: internal.buttonHeight
        anchors {bottom: parent.bottom; bottomMargin: internal.margins; horizontalCenter: parent.horizontalCenter}
        iconLabel { text: Icons.faChevronCircleRight; color: mainRect.color }
        text: "Criar partida"

        onPressed:{
            Core.gameManager.createMatch(
                        matchNameInput.text,
                        Core.playerModel.nickname,
                        Core.playerModel.email);

            Core.playerModel.isMatchCreator = true;

            mainStackView.replace("qrc:/views/PlayerApproval.qml")
        }
    }
}
