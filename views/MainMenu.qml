import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "qrc:/components"
import "qrc:/FontAwesome"

Item {
    ColumnLayout {
        width: parent.width
        anchors { verticalCenter: parent.verticalCenter; verticalCenterOffset: -haqtonLabel.height }
        HaQtonLabel {
            id: haqtonLabel
            Layout.topMargin: 8*internal.margins
            font { family: gameFont.name; pixelSize: 64 }
            Layout.alignment: Qt.AlignHCenter
        }
        GridLayout {
            columns: 2; columnSpacing: internal.margins/2; rowSpacing: internal.margins/2
            Layout.fillWidth: true; Layout.fillHeight: false
            Repeater {
                model: [
                    {
                        icon: Icons.faPlusCircle,
                        text: "Criar uma partida",
                        targetView: "qrc:/views/MatchCreation.qml"
                    },
                    {
                        icon: Icons.faGamepad,
                        text: "Participar de uma partida",
                        targetView: "qrc:/views/MatchSelection.qml"
                    },
                    {
                        icon: Icons.faDiceD20,
                        text: "Sobre o HaQton",
                        targetView: "qrc:/views/About.qml"
                    },
                    {
                        icon: Icons.faMedal,
                        text: "Patrocinadores",
                        targetView: "qrc:/views/Sponsors.qml"
                    }
                ]
                GameButton {
                    Layout.fillWidth: true; Layout.fillHeight: true
                    iconLabel { text: modelData.icon; color: mainRect.color }
                    iconLabelSize: 64
                    text: modelData.text
                    onPressed: mainStackView.replace(modelData.targetView)
                }
            }
        }
    }
}
