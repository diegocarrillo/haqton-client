import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "qrc:/components"
import "qrc:/FontAwesome"
import solutions.qmob.haqton 1.0

Item {

    StackView.onActivated: {
        Core.gameManager.getMatchesWaitingForPlayers()
    }

    ReturnButton{
        id: returnButton
        anchors{top:parent.top; left: parent.left}
        onClicked: mainStackView.replace("qrc:/views/MainMenu.qml");
    }

    ColumnLayout {
        width: parent.width
        anchors{top: returnButton.bottom; topMargin: internal.margins; bottom: parent.bottom}
        spacing: internal.margins

        Label{
            id: textLabel
            Layout.fillWidth: true
            Layout.preferredHeight: 40
            horizontalAlignment: Text.AlignHLeft

            text: "Escolha uma partida:"
            color: "white"
            wrapMode: Text.WordWrap
            font.capitalization: Font.AllUppercase
            font { family: gameFont.name; pixelSize: 40}
        }

        ListView
        {
            id: matchesList
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            model: Core.gameManager.context.matches
            clip: true

            ScrollBar.vertical: ScrollBar {}

            delegate: Button {
                width: matchesList.width
                height: 100
                Material.background: "white"

                contentItem: Item {
                    ColumnLayout
                    {
                        id: textColumn
                        anchors{top: parent.top; topMargin: 5;
                            bottom: parent.bottom; bottomMargin: 5;
                            left: parent.left; leftMargin: 5}

                        spacing: 0

                        Text {
                            text: modelData.description
                            font.pixelSize: 20
                        }
                        Text {
                            text: "Criador: " + modelData.creator_name
                            font.pixelSize: 12

                        }
                        Text {
                            text: "Número de jogadores: " + modelData.match_players.length
                            font.pixelSize: 12
                        }
                        Text {
                            text: "ID: " + modelData.id
                            font.pixelSize: 10
                        }
                    }

                    Label{
                        id: icon
                        anchors{
                            right: parent.right; rightMargin: internal.margins
                            verticalCenter: parent.verticalCenter
                        }
                        width: height

                        text: Icons.faPlayCircle
                        color: mainRect.color
                        font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 64 }
                    }
                }

                onClicked: {
                    Core.gameManager.addPlayerToMatch(modelData.id,
                                                      Core.playerModel.nickname,
                                                      Core.playerModel.email)
                    Core.playerModel.isMatchCreator = false;

                    mainStackView.replace("qrc:/views/PlayerWait.qml")
                }
            }
        }
    }
}
