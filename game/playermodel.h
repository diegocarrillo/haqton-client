#ifndef PLAYERMODEL_H
#define PLAYERMODEL_H

#include <QObject>

/**
 * @brief Class that holds information about the player's inputs at the game
 */
class PlayerModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nickname READ nickname WRITE setNickname NOTIFY nicknameChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(bool isMatchCreator READ isMatchCreator WRITE setIsMatchCreator NOTIFY matchCreatorChanged)

public:
    explicit PlayerModel(QObject *parent = nullptr);

    QString nickname() const;
    void setNickname(const QString &nickname);

    QString email() const;
    void setEmail(const QString &email);

    bool isMatchCreator() const;
    void setIsMatchCreator(bool isMatchCreator);

signals:
    void nicknameChanged();
    void emailChanged();
    void matchCreatorChanged();

private:

    QString _nickname;
    QString _email;
    bool _isMatchCreator;
};

#endif // PLAYERMODEL_H
