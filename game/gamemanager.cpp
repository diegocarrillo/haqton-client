#include "gamemanager.h"

#include <QtQml>

Q_LOGGING_CATEGORY(gamemanager, "solutions.qmob.haqton.gamemanager")

GameManager::GameManager(QObject *parent) : QObject(parent),
    _context(new QQmlPropertyMap(this)),
    _isLoading(false)
{
    qmlRegisterInterface<GameManager>("GameManager", 1);

    //Create context topics
    _context->insert(GAME_CONTEXT_TOPIC_MATCHES, "");
    _context->insert(GAME_CONTEXT_TOPIC_CURRENT_MATCH, "");
    _context->insert(GAME_CONTEXT_TOPIC_QUESTION, "");
    _context->insert(GAME_CONTEXT_TOPIC_CURRENT_ROUND, "");
    _context->insert(GAME_CONTEXT_TOPIC_MATCH_SUMMARY, "");

}

void GameManager::getMatchesWaitingForPlayers()
{
    //Reset matches context topic
    _context->insert(GAME_CONTEXT_TOPIC_MATCHES, "");

    _isLoading = true;
    emit isLoadingChanged();

    NetworkReply *reply = _networkController->get("matches");

    connect(reply, &NetworkReply::finished, [=](QJsonValue jsonValue){
        //Add response to matches context topic
        _context->insert(GAME_CONTEXT_TOPIC_MATCHES, QVariant::fromValue(jsonValue.toArray()));
        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();
    });
}

void GameManager::createMatch(QString matchName, QString creatorNickname, QString creatorEmail)
{
    //Reset relevant context topics
    _context->insert(GAME_CONTEXT_TOPIC_CURRENT_MATCH, "");
    _context->insert(GAME_CONTEXT_TOPIC_QUESTION, "");
    _context->insert(GAME_CONTEXT_TOPIC_CURRENT_ROUND, "");

    //Prepare http request
    QJsonObject jsonContent;
    jsonContent.insert("description", matchName);
    jsonContent.insert("nickname", creatorNickname);
    jsonContent.insert("email", creatorEmail);

    NetworkReply *reply = _networkController->post("matches", jsonContent);

    _isLoading = true;
    emit isLoadingChanged();

    connect(reply, &NetworkReply::finished, [=](QJsonValue jsonValue){
        //Add response to current match context topic
        QJsonObject jsonObject = jsonValue.toObject();
        _context->insert(GAME_CONTEXT_TOPIC_CURRENT_MATCH, QVariant::fromValue(jsonObject));
        _messagingController->setTopic(QString::number(jsonObject.value("topic").toDouble()));
        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();
    });
}

void GameManager::deleteMatch(int matchId)
{
    NetworkReply *reply = _networkController->del("matches/" + QString::number(matchId));

    _isLoading = true;
    emit isLoadingChanged();

    connect(reply, &NetworkReply::finished, [=](){
        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();
    });
}

void GameManager::updatePlayerStatus(int matchId, int playerId, bool isPlayerApproved)
{
    //Prepare http request
    QJsonObject jsonContent;
    jsonContent.insert("approved", isPlayerApproved);

    NetworkReply *reply = _networkController->put("matches/" + QString::number(matchId) +
                                                  "/players/" + QString::number(playerId),
                                                  jsonContent);

    _isLoading = true;
    emit isLoadingChanged();

    connect(reply, &NetworkReply::finished, [=](){
        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();
    });
}

void GameManager::addPlayerToMatch(int matchId, QString playerNickname, QString playerEmail)
{
    //Reset relevant context topics
    _context->insert(GAME_CONTEXT_TOPIC_CURRENT_MATCH, "");
    _context->insert(GAME_CONTEXT_TOPIC_QUESTION, "");
    _context->insert(GAME_CONTEXT_TOPIC_CURRENT_ROUND, "");

    //Prepare http request
    QJsonObject jsonContent;
    jsonContent.insert("nickname", playerNickname);
    jsonContent.insert("email", playerEmail);

    NetworkReply *reply = _networkController->post("matches/" + QString::number(matchId) + "/players",
                                                   jsonContent);

    _isLoading = true;
    emit isLoadingChanged();

    connect(reply, &NetworkReply::finished, [=](QJsonValue jsonValue){
        QJsonArray playersArray = jsonValue.toArray();
        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();

        //Get matches array from game context
        QJsonArray matches = _context->value(GAME_CONTEXT_TOPIC_MATCHES).toJsonArray();

        //Search array for the current match id
        for(int i = 0; i < matches.size(); i++)
        {
            QJsonObject match = matches.at(i).toObject();
            if(match.value("id").toInt() == matchId)
            {
                //Add player to current match at the game context
                match.insert("match_players", playersArray);
                _context->insert(GAME_CONTEXT_TOPIC_CURRENT_MATCH, QVariant::fromValue(match));
                _messagingController->setTopic(QString::number(match.value("topic").toDouble()));
                break;
            }
        }
    });
}

void GameManager::startMatch(int matchId)
{
    //Prepare http request
    QJsonObject jsonContent;
    jsonContent.insert("status", 1);

    NetworkReply *reply = _networkController->put("matches/" + QString::number(matchId), jsonContent);

    _isLoading = true;
    emit isLoadingChanged();

    connect(reply, &NetworkReply::finished, [=](QJsonValue jsonValue){

        Q_UNUSED(jsonValue);

        //Send a request to get the first match question
        getRandomQuestion(matchId);

        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();
    });
}

void GameManager::endMatch(int matchId)
{
    //Prepare http request
    QJsonObject jsonContent;
    jsonContent.insert("status", 2);

    NetworkReply *reply = _networkController->put("matches/" + QString::number(matchId), jsonContent);

    _isLoading = true;
    emit isLoadingChanged();

    connect(reply, &NetworkReply::finished, [=](QJsonValue jsonValue){

        Q_UNUSED(jsonValue);

        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();
    });
}

void GameManager::getRandomQuestion(int matchId)
{
    //Reset question context topic
    _context->insert(GAME_CONTEXT_TOPIC_QUESTION, "");

    NetworkReply *reply = _networkController->get("matches/" + QString::number(matchId) + "/random_question");

    _isLoading = true;
    emit isLoadingChanged();

    connect(reply, &NetworkReply::finished, [=](QJsonValue jsonValue){
        //Add response to question context topic
        _context->insert(GAME_CONTEXT_TOPIC_QUESTION, QVariant::fromValue(jsonValue.toObject()));
        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();
    });
}

void GameManager::processPlayerAnswer(int matchId, int playerId, int questionId, int playerOption)
{
    //Prepare http request
    QJsonObject jsonContent;
    jsonContent.insert("question_id", questionId);
    jsonContent.insert("player_option", playerOption);

    NetworkReply *reply = _networkController->post("matches/" + QString::number(matchId) +
                                                   "/players/" + QString::number(playerId) +
                                                   "/answer", jsonContent);

    _isLoading = true;
    emit isLoadingChanged();

    connect(reply, &NetworkReply::finished, [=](QJsonValue jsonValue){
        Q_UNUSED(jsonValue);
        reply->deleteLater();

        _isLoading = false;
        emit isLoadingChanged();
    });
}

QQmlPropertyMap *GameManager::context() const
{
    return _context;
}

void GameManager::onNewMessageReceived(const QString &message)
{
    QJsonDocument jsonMessage = QJsonDocument::fromJson(message.toUtf8());
    QJsonObject jsonObject = jsonMessage.object();

    QString messageTitle = jsonObject.value("message").toString();

    //For each message topic:
    if(messageTitle == "matches_update")
    {
        //Updates matches context topic
        _context->insert(GAME_CONTEXT_TOPIC_MATCHES, QVariant::fromValue(jsonObject.value("data")));
    }

    else if(messageTitle == "players_update")
    {
        //Update current match context topic with received players array
        QJsonObject currentMatchObject = _context->value(GAME_CONTEXT_TOPIC_CURRENT_MATCH).toJsonObject();
        currentMatchObject.insert("match_players", jsonObject.value("data"));

        _context->insert(GAME_CONTEXT_TOPIC_CURRENT_MATCH, currentMatchObject);
    }

    else if(messageTitle == "new_question")
    {
        //Update question context topic with received question data
        _context->insert(GAME_CONTEXT_TOPIC_QUESTION, QVariant::fromValue(jsonObject.value("data")));

        //Get players array from current match
        QJsonObject currentMatch = _context->value(GAME_CONTEXT_TOPIC_CURRENT_MATCH).toJsonObject();
        QJsonArray players = currentMatch.value("match_players").toArray();

        //Create a context array to indicate each player's information during the current round
        QJsonArray currentRoundStatus = _context->value(GAME_CONTEXT_TOPIC_CURRENT_ROUND).toJsonArray();

        //If this is the fisrt time that currentRound context is used, create all its sub values
        if(currentRoundStatus.isEmpty())
        {
            for(int i = 0; i < players.size(); i++)
            {
                QJsonObject currentPlayer = players.at(i).toObject();
                QJsonObject playerStatus;
                playerStatus.insert("id", currentPlayer.value("id"));
                playerStatus.insert("player_name", currentPlayer.value("player_name"));
                playerStatus.insert("has_played", false);
                playerStatus.insert("question_answered", -1);
                playerStatus.insert("current_score", 0);

                currentRoundStatus.append(playerStatus);
            }
        }
        //If currentRound is already created, only reset its relevant sub values
        else
        {
            for(int i = 0; i < currentRoundStatus.size(); i++)
            {
                QJsonObject playerStatus = currentRoundStatus.at(i).toObject();
                playerStatus.insert("has_played", false);
                playerStatus.insert("question_answered", -1);

                currentRoundStatus.replace(i, playerStatus);
            }
        }

        //Update current round context topic
        _context->insert(GAME_CONTEXT_TOPIC_CURRENT_ROUND, currentRoundStatus);

        emit newQuestionReceived();
    }

    else if(messageTitle == "new_answer")
    {
        QJsonObject messageData = jsonObject.value("data").toObject();
        QJsonArray currentRound = _context->value(GAME_CONTEXT_TOPIC_CURRENT_ROUND).toJsonArray();
        QJsonObject question = _context->value(GAME_CONTEXT_TOPIC_QUESTION).toJsonObject();

        for(int i = 0; i < currentRound.size(); i++)
        {
            //Find player at the current round array
            if(currentRound.at(i).toObject().value("id") == messageData.value("player_id"))
            {
                //Get current player object
                QJsonObject updatedRoundData = currentRound.at(i).toObject();

                //Update the flag indicating that the player has answered the quetion for the current round
                updatedRoundData.insert("has_played", true);
                updatedRoundData.insert("question_answered", messageData.value("player_option"));

                // Check if answer is correct
                if(messageData.value("player_option") == question.value("right_option"))
                {
                    //Update player's score
                    int newScore = currentRound.at(i).toObject().value("current_score").toDouble() + 1;
                    updatedRoundData.insert("current_score", newScore);
                }

                currentRound.replace(i, updatedRoundData);

                break;
            }
        }

        //Update current round context
        _context->insert(GAME_CONTEXT_TOPIC_CURRENT_ROUND, currentRound);
    }

    else if(messageTitle == "match_finished")
    {
        // Create a match summary array based on the last round
        QJsonArray matchSummary = _context->value(GAME_CONTEXT_TOPIC_CURRENT_ROUND).toJsonArray();

        //Sort players from the match summary based on their score,
        //using the bubble sort algorithm
        for(int i = 0; i < matchSummary.size(); i++)
        {
            for(int j = 0; j < i; j++)
            {
                if(matchSummary.at(i).toObject().value("current_score").toInt() >
                        matchSummary.at(j).toObject().value("current_score").toInt())
                {
                    //If player i has a higher score than player j,
                    //swap them at the match summary
                    QJsonObject aux = matchSummary.at(i).toObject();
                    matchSummary.replace(i, matchSummary.at(j));
                    matchSummary.replace(j, aux);
                }
            }
        }

        //After sort, first player will have the higher score
        int highestScore = matchSummary.first().toObject().value("current_score").toInt();

        //Set a "winner" flag for all the players with the highest score
        for(int i = 0; i < matchSummary.size(); i++)
        {
            bool isWinner = (matchSummary.at(i).toObject().value("current_score").toInt() == highestScore) &&
                    (highestScore > 0);

            QJsonObject playerStatus = matchSummary.at(i).toObject();
            playerStatus.insert("is_winner", isWinner);
            matchSummary.replace(i, playerStatus);
        }

        //Add match summary to context
        _context->insert(GAME_CONTEXT_TOPIC_MATCH_SUMMARY, matchSummary);

        emit matchFinished();

        //Reset messaging controller topic
        _messagingController->setTopic(QStringLiteral("matches"));
    }
}

void GameManager::setNetworkController(NetworkController *networkController)
{
    _networkController = networkController;
}

void GameManager::setMessagingController(MessagingController *messagingController)
{
    _messagingController = messagingController;

    connect(_messagingController, &MessagingController::newMessage,
            this, &GameManager::onNewMessageReceived);
}

bool GameManager::isLoading()
{
    return _isLoading;
}

