#include "playermodel.h"

#include <QtQml>

PlayerModel::PlayerModel(QObject *parent) : QObject(parent),
    _isMatchCreator(false)
{
    qmlRegisterInterface<PlayerModel>("PlayerModel", 1);
}

QString PlayerModel::nickname() const
{
    return _nickname;
}

void PlayerModel::setNickname(const QString &nickname)
{
    _nickname = nickname;
}

QString PlayerModel::email() const
{
    return _email;
}

void PlayerModel::setEmail(const QString &email)
{
    _email = email;
}

bool PlayerModel::isMatchCreator() const
{
    return _isMatchCreator;
}

void PlayerModel::setIsMatchCreator(bool isMatchCreator)
{
    _isMatchCreator = isMatchCreator;
}
