#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <QObject>
#include <QQmlPropertyMap>
#include "network/networkcontroller.h"
#include "messaging/messagingcontroller.h"

#define GAME_CONTEXT_TOPIC_MATCHES          "matches"
#define GAME_CONTEXT_TOPIC_CURRENT_MATCH    "currentMatch"
#define GAME_CONTEXT_TOPIC_QUESTION         "question"
#define GAME_CONTEXT_TOPIC_CURRENT_ROUND    "currentRound"
#define GAME_CONTEXT_TOPIC_MATCH_SUMMARY    "matchSummary"

/**
 * @brief Class that provides an interface for nthe UI to access the game API
 * and receive updates from it
 */
class GameManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlPropertyMap * context READ context CONSTANT)
    Q_PROPERTY(bool isLoading READ isLoading NOTIFY isLoadingChanged)

public:

    explicit GameManager(QObject *parent = nullptr);
    ~GameManager() Q_DECL_OVERRIDE = default;

    /**
     * @brief Gets a list of all the matches currently waiting for players to join and
     * appends it to the game context
     */
    Q_INVOKABLE void getMatchesWaitingForPlayers();

    /**
     * @brief Creates a new match and adds the new match information to the game context
     * @param matchName
     * @param creatorNickname
     * @param creatorEmail
     */
    Q_INVOKABLE void createMatch(QString matchName,
                                 QString creatorNickname,
                                 QString creatorEmail);

    /**
     * @brief Deletes an existing match
     * @param matchId
     */
    Q_INVOKABLE void deleteMatch(int matchId);

    /**
     * @brief Updates a match's player status
     * @param matchId
     * @param playerId
     * @param isPlayerApproved
     */
    Q_INVOKABLE void updatePlayerStatus(int matchId,
                                        int playerId,
                                        bool isPlayerApproved);

    /**
     * @brief Adds a new player to a match and updates the game context with the new player's information
     * @param matchId
     * @param playerNickname
     * @param playerEmail
     */
    Q_INVOKABLE void addPlayerToMatch(int matchId,
                                      QString playerNickname,
                                      QString playerEmail);


    /**
     * @brief Starts a match
     * @param matchId
     */
    Q_INVOKABLE void startMatch(int matchId);

    /**
     * @brief Ends a match
     * @param matchId
     */
    Q_INVOKABLE void endMatch(int matchId);

    /**
     * @brief Get a new question to the current match and appends it to the game context
     * @param matchId
     */
    Q_INVOKABLE void getRandomQuestion(int matchId);

    /**
     * @brief Processes an anwer of a players
     * @param matchId
     * @param playerId
     * @param questionId
     * @param playerOption
     */
    Q_INVOKABLE void processPlayerAnswer(int matchId,
                                         int playerId,
                                         int questionId,
                                         int playerOption);

    /**
     * @brief Exposes the game context to the QML
     * @return
     */
    QQmlPropertyMap *context() const;

    /**
     * @brief Returns true to the view when the game manager is waiting for a request
     * and processing datas
     * @return
     */
    bool isLoading();


    /**
     * @brief Injects a MessagingController instance
     * @param messagingController
     */
    void setMessagingController(MessagingController *messagingController);

    /**
     * @brief Injects a NetworkController instance
     * @param NetworkController
     */
    void setNetworkController(NetworkController *networkController);

signals:

    /**
     * @brief Emits signal to the QML context when a new question is available
     */
    void newQuestionReceived();

    /**
     * @brief Emits signal to the QML context when the match finishes
     */
    void matchFinished();

    /**
     * @brief Emits signal to the QML when the loading flag changes
     */
    void isLoadingChanged();

private slots:

    /**
     * @brief Deals with received messages from the messaging controller
     * @param message
     */
    void onNewMessageReceived(const QString &message);

private:

    QQmlPropertyMap *_context; /**< Map that exposes the game context to the UI*/
    NetworkController *_networkController; /**< NetworkController instance*/
    MessagingController *_messagingController; /**< MessagingController instance*/

    bool _isLoading; /**< Flag that holds the loading status of the game*/
};

#endif // GAMEMANAGER_H
