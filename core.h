/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef CORE_H_
#define CORE_H_

#include <QObject>

class NetworkController;
class MessagingController;
class GameManager;
class PlayerModel;

class Core : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MessagingController * messagingController READ messagingController CONSTANT)
    Q_PROPERTY(GameManager * gameManager READ gameManager CONSTANT)
    Q_PROPERTY(PlayerModel * playerModel READ playerModel CONSTANT)

public:
    ~Core() Q_DECL_OVERRIDE;

    static Core *instance();

    MessagingController *messagingController() const;

    GameManager *gameManager() const;

    PlayerModel *playerModel() const;

private:
    explicit Core(QObject *parent = nullptr);

    static Core *_instance;
    NetworkController *_networkController;
    MessagingController *_messagingController;
    GameManager *_gameManager;
    PlayerModel *_playerModel;
};

#endif	// CORE_H_
