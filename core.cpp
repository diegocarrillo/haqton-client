/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "core.h"
#include "network/networkcontroller.h"
#include "messaging/messagingcontroller.h"
#include "game/gamemanager.h"
#include "game/playermodel.h"

Q_LOGGING_CATEGORY(core, "solutions.qmob.haqton.core")

Core *Core::_instance = nullptr;

Core::Core(QObject *parent)
    : QObject(parent),
      _networkController(new NetworkController(this)),
      _messagingController(new MessagingController(this)),
      _gameManager(new GameManager(this)),
      _playerModel(new PlayerModel(this))
{
    // Inject objects into game manager
    _gameManager->setNetworkController(_networkController);
    _gameManager->setMessagingController(_messagingController);
}

PlayerModel *Core::playerModel() const
{
    return _playerModel;
}

Core::~Core()
{
    delete _messagingController;
}

Core *Core::instance()
{
    if (!_instance) {
        _instance = new Core;
    }
    return _instance;
}

MessagingController *Core::messagingController() const
{
    return _messagingController;
}

GameManager *Core::gameManager() const
{
    return _gameManager;
}
