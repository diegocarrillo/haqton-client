/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef ZHELPERS_H
#define ZHELPERS_H

#include <zmq.hpp>

inline static zmq::send_result_t s_sendmore (zmq::socket_t &socket, const std::string &string) {
    zmq::message_t message(string.size());
    memcpy (message.data(), string.data(), string.size());

    auto result = socket.send (message, zmq::send_flags::sndmore);
    return result;
}

inline static zmq::send_result_t s_send (zmq::socket_t &socket, const std::string &string, zmq::send_flags flags = zmq::send_flags::none) {
    zmq::message_t message(string.size());
    memcpy (message.data(), string.data(), string.size());

    auto result = socket.send (message, flags);
    return result;
}

inline static std::string s_recv (zmq::socket_t &socket, zmq::recv_flags flags = zmq::recv_flags::none) {
    zmq::message_t message;
    zmq::recv_result_t result = socket.recv(message, flags);

    return std::string(static_cast<char*>(message.data()), message.size());
}

#endif // ZHELPERS_H
