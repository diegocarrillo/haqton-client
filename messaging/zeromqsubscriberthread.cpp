/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "zeromqsubscriberthread.h"

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(messagingcontroller)

ZeroMQSubscriberThread::ZeroMQSubscriberThread(QString url, QObject *parent)
    : QThread(parent),
      _url(std::move(url)),
      _context(1),
      _subscriber(_context, ZMQ_SUB)
{
}

void ZeroMQSubscriberThread::run()
{
    _subscriber.connect(_url.toStdString());

    qCDebug(messagingcontroller) << "Starting subscriber thread";
    while(true)
    {
        //  Wait for next request from client
        std::string topic = s_recv(_subscriber);
        std::string string = s_recv(_subscriber);

        qCDebug(messagingcontroller) << "New message:" << QString::fromStdString(string);
        Q_EMIT newMessage(QString::fromStdString(string));
    }
}

void ZeroMQSubscriberThread::setTopic(QString topic)
{
    if (!_topic.isEmpty()) {
        _subscriber.setsockopt(ZMQ_UNSUBSCRIBE, _topic.toStdString().c_str(), _topic.toStdString().size());
    }
    _topic = std::move(topic);
    _subscriber.setsockopt(ZMQ_SUBSCRIBE, _topic.toStdString().c_str(), _topic.toStdString().size());
    qCDebug(messagingcontroller) << "Subscriber topic changed to" << _topic;
}
