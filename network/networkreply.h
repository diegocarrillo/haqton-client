#ifndef NETWORKREPLY_H
#define NETWORKREPLY_H

#include <QObject>
#include <QtNetwork>

/**
 * @brief Class that provides an abstraction to HTTP replies from NetworkController
 */
class NetworkReply : public QObject
{
    Q_OBJECT

public:
     explicit NetworkReply(QNetworkReply *networkReply, QObject *parent = nullptr);

signals:
    void finished(const QJsonValue &QJsonValue);
};

#endif // NETWORKREPLY_H
