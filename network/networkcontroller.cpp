#include "networkcontroller.h"
#include <QFile>

NetworkController::NetworkController(QObject *parent) : QObject(parent)
{
    // Read token file content
    QFile file(":/auth/token.jwt");
    if (file.open(QFile::ReadOnly | QFile::Text))
    {
        QTextStream in(&file);
        _bearerToken.append("Bearer ");
        _bearerToken.append(in.readAll());

        // Remove any line break that could be inserted while reading the text stream
        _bearerToken.remove("\n");
    }
}

NetworkReply *NetworkController::get(const QString &endpoint)
{
    QNetworkRequest request;
    request.setUrl(QUrl(_RESTFULSERVERURL + endpoint));
    request.setRawHeader(QByteArray("Authorization"), QByteArray(_bearerToken.toUtf8()));

    QNetworkReply *reply = _networkAccessManager.get(request);

    return new NetworkReply(reply);
}

NetworkReply *NetworkController::post(const QString &endpoint, const QJsonObject &resourceData)
{
    QNetworkRequest request;
    request.setUrl(QUrl(_RESTFULSERVERURL + endpoint));
    request.setRawHeader(QByteArray("Authorization"), QByteArray(_bearerToken.toUtf8()));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QByteArray data = QJsonDocument(resourceData).toJson();

    QNetworkReply *reply = _networkAccessManager.post(request, data);

    return new NetworkReply(reply);
}

NetworkReply *NetworkController::put(const QString &endpoint, const QJsonObject &resourceData)
{
    QByteArray jsonData = QJsonDocument(resourceData).toJson();

    QNetworkRequest request;
    request.setUrl(QUrl(_RESTFULSERVERURL + endpoint));
    request.setRawHeader(QByteArray("Authorization"), QByteArray(_bearerToken.toUtf8()));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QString("application/json"));

    QNetworkReply *reply = _networkAccessManager.put(request, jsonData);

    return new NetworkReply(reply);
}

NetworkReply *NetworkController::del(const QString &endpoint)
{
    QNetworkRequest request;
    request.setUrl(QUrl(_RESTFULSERVERURL + endpoint));
    request.setRawHeader(QByteArray("Authorization"), QByteArray(_bearerToken.toUtf8()));

    QNetworkReply *reply = _networkAccessManager.deleteResource(request);

    return new NetworkReply(reply);
}
