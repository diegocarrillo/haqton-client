#ifndef NETWORKCONTROLLER_H
#define NETWORKCONTROLLER_H

#include <QObject>
#include <QtNetwork>
#include "networkreply.h"

/**
 * @brief Class that provides access to basic HTTP methods to a specific server
 */
class NetworkController : public QObject
{
    Q_OBJECT

public:
    explicit NetworkController(QObject *parent = nullptr);

public:
    NetworkReply *get(const QString &endpoint);

    NetworkReply *post(const QString &endpoint, const QJsonObject &resourceData);

    NetworkReply *put(const QString &endpoint, const QJsonObject &resourceData);

    NetworkReply *del(const QString &endpoint);

private:
    QString _RESTFULSERVERURL = QStringLiteral("https://haqton.qmob.solutions/");

    QNetworkAccessManager _networkAccessManager;
    QString _bearerToken;
};

#endif // NETWORKCONTROLLER_H
