#include "networkreply.h"

Q_LOGGING_CATEGORY(networkreply, "solutions.qmob.haqton.networkreply")

NetworkReply::NetworkReply(QNetworkReply *networkReply, QObject *parent) : QObject(parent)
{
    connect(networkReply, &QNetworkReply::finished, [=](){

        QString strReply = (QString)networkReply->readAll();
        qCDebug(networkreply)<< "Response:" << strReply;
        QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());

        QJsonValue jsonContent;

        if(jsonResponse.isArray())
        {
            jsonContent = QJsonValue(jsonResponse.array());
        }
        else if(jsonResponse.isObject())
        {
            jsonContent = QJsonValue(jsonResponse.object());
        }

        emit finished(jsonContent);

        networkReply->deleteLater();
    });

    connect(networkReply, &QNetworkReply::errorOccurred, [=](){

        QString strReply = (QString)networkReply->readAll();
        qCDebug(networkreply)<< "Error:" << networkReply->error();
        networkReply->deleteLater();
    });
}
