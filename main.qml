/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15
import QtQuick.Window 2.15

import solutions.qmob.haqton 1.0
import "qrc:/views"
import "qrc:/FontAwesome"

ApplicationWindow {

    // Adapt to device's screen resolution, but limit width to 400
    width: Screen.width > 400 ? 400 : Screen.width
    height: Screen.width > 400 ? width*16/9 : Screen.height

    visible: true
    title: qsTr("HaQton")

    FontLoader { id: gameFont; source: "assets/fonts/PocketMonk-15ze.ttf" }

    Audio { source: "assets/audio/Magic-Clock-Shop_Looping.mp3"; loops: Audio.Infinite; autoPlay: true }

    QtObject {
        id: internal
        property int margins: 10
        property int buttonHeight: 100
    }
    
    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: "#44a8e4"

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#44a8e4"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#f79154"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
        }
    }

    StackView {
        id: mainStackView
        visible: !Core.gameManager.isLoading
        anchors { fill: parent; margins: internal.margins }
        initialItem: "qrc:/views/UserInput.qml"
    }

    BusyIndicator {
        id: busyIndicator
        visible: Core.gameManager.isLoading
        anchors { centerIn: parent;}

        contentItem: Label {
            id: spinnerIcon
            font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 60}
            text: Icons.faSpinner
            color: "white"
        }

        RotationAnimator {
            target: spinnerIcon
            running: busyIndicator.visible && busyIndicator.running
            from: 0
            to: 360
            loops: Animation.Infinite
            duration: 2000
        }
    }
}
